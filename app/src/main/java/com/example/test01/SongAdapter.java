package com.example.test01;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
public class SongAdapter extends RecyclerView.Adapter<SongViewHolder> {

    SongAdapter(@NonNull Context context, @NonNull ArrayList<Music> songs, @NonNull MainActivity mainActivity) {
        this.context = context;
        this.songs = songs;
        this.mainActivity = mainActivity;
    }


    @NonNull
    @Override
    public SongViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //called when creating creating view holder
        LayoutInflater inflater = LayoutInflater.from(context);
        View ItemView = inflater.inflate(R.layout.item_song, parent, false);
        SongViewHolder viewHolder = new SongViewHolder(ItemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SongViewHolder holder, int position) {
        //reused, repopulate view holder
        Music song = songs.get(position);
        holder.imageView.setImageResource(song.image);
        holder.artistNameView.setText(song.artist);
        holder.songNameView.setText(song.name);

        if (position == mainActivity.currentSongIndex) {
            holder.selectedBackGround.setVisibility(View.VISIBLE);

        } else {
            holder.selectedBackGround.setVisibility(View.INVISIBLE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("user tapped at index:" + position);
                mainActivity.onUserSelectSongAtPosition(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        //how many items to display
        return songs.size();
    }
    Context context;
    ArrayList<Music> songs;
    MainActivity mainActivity;
}
