package com.example.test01;
// imports
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    int selectedPlaylist;
    boolean playlistButton = false;
    // tell user selected song at position
    void onUserSelectSongAtPosition(int position) {
        switchSong(currentSongIndex, position);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playlists);
        connectPlaylistViews();
        playlistButtonHandlers();
        // main code
    }
    void playlistButtonHandlers() {
        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPlaylist = 1;
                System.out.println("main");
                setContentView(R.layout.activity_main);
                populateDataModel();

                connectXMLViews();
                setupRecyclerView();
                displayCurrentSong();

                setupButtonHandlers();
            }
        });
        jazz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setContentView(R.layout.activity_main);
                selectedPlaylist = 2;
                System.out.println("jazz");
                populateDataModel();

                connectXMLViews();
                setupRecyclerView();
                displayCurrentSong();

                setupButtonHandlers();
            }
        });
        pop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPlaylist = 3;
                System.out.println("pop");
                setContentView(R.layout.activity_main);
                populateDataModel();

                connectXMLViews();
                setupRecyclerView();
                displayCurrentSong();

                setupButtonHandlers();
            }
        });
        rock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPlaylist = 4;
                System.out.println("rock");
                setContentView(R.layout.activity_main);
                populateDataModel();

                connectXMLViews();
                setupRecyclerView();
                displayCurrentSong();

                setupButtonHandlers();
            }
        });
        workout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPlaylist = 6;
                System.out.println("workout");
                setContentView(R.layout.activity_main);
                populateDataModel();

                connectXMLViews();
                setupRecyclerView();
                displayCurrentSong();

                setupButtonHandlers();
            }
        });
        meditate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPlaylist = 5;
                System.out.println("meditate");
                setContentView(R.layout.activity_main);
                populateDataModel();

                connectXMLViews();
                setupRecyclerView();
                displayCurrentSong();

                setupButtonHandlers();
            }
        });
        rap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPlaylist = 7;
                System.out.println("rap");
                setContentView(R.layout.activity_main);
                populateDataModel();

                connectXMLViews();
                setupRecyclerView();
                displayCurrentSong();

                setupButtonHandlers();
            }
        });
    }
    void setupRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        playlistRecyclerView.setLayoutManager(layoutManager);

        // connect the adapter to the recyvlerview
        songAdapter = new SongAdapter(this, playlist.songs, this);
        playlistRecyclerView.setAdapter(songAdapter);

    }
    void populateDataModel() {
        switch (selectedPlaylist) {

            case 1:
                System.out.println("Main");
                // playlist
                playlist.name = "Default";
                playlist.songs = new ArrayList<Music>();
                // songs
                Music song = new Music();
                song.name = "Acoustic Breeze";
                song.artist = "bensound.com";
                song.image = R.drawable.acousticbreeze;
                song.source = R.raw.acousticbreeze;
                playlist.songs.add(song);

                song = new Music();
                song.name = "A New Beginning";
                song.artist = "bensound.com";
                song.image = R.drawable.anewbeginning;
                song.source = R.raw.anewbeginning;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Summer";
                song.artist = "bensound.com";
                song.image = R.drawable.summer;
                song.source = R.raw.summer;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Hey";
                song.artist = "bensound.com";
                song.image = R.drawable.hey;
                song.source = R.raw.hey;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Happy rock";
                song.artist = "bensound.com";
                song.image = R.drawable.happyrock;
                song.source = R.raw.happyrock;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Creative Minds";
                song.artist = "bensound.com";
                song.image = R.drawable.creativeminds;
                song.source = R.raw.creativeminds;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Going higher";
                song.artist = "bensound.com";
                song.image = R.drawable.goinghigher;
                song.source = R.raw.goinghigher;
                playlist.songs.add(song);
                break;

            case 2:
                System.out.println("Jazz");
                // playlist
                playlist.name = "Jazz";
                playlist.songs = new ArrayList<Music>();
                // songs
                song = new Music();
                song.name = "Going higher";
                song.artist = "bensound.com";
                song.image = R.drawable.jazz;
                song.source = R.raw.jazz1;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Going higher";
                song.artist = "bensound.com";
                song.image = R.drawable.jazz;
                song.source = R.raw.jazz2;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Going higher";
                song.artist = "bensound.com";
                song.image = R.drawable.jazz;
                song.source = R.raw.jazz3;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Going higher";
                song.artist = "bensound.com";
                song.image = R.drawable.jazz;
                song.source = R.raw.jazz4;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Going higher";
                song.artist = "bensound.com";
                song.image = R.drawable.jazz;
                song.source = R.raw.jazz5;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Going higher";
                song.artist = "bensound.com";
                song.image = R.drawable.jazz;
                song.source = R.raw.jazz6;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Going higher";
                song.artist = "bensound.com";
                song.image = R.drawable.jazz;
                song.source = R.raw.jazz7;
                playlist.songs.add(song);

                // i miscounted/misnamed the file oh my god i
                // give up

                song = new Music();
                song.name = "Going higher";
                song.artist = "bensound.com";
                song.image = R.drawable.jazz;
                song.source = R.raw.jazz9;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Going higher";
                song.artist = "bensound.com";
                song.image = R.drawable.jazz;
                song.source = R.raw.jazz10;
                playlist.songs.add(song);
                break;

            case 3:
                // playlist
                playlist.name = "Pop";
                playlist.songs = new ArrayList<Music>();
                // songs
                System.out.println("Pop");

                song = new Music();
                song.name = "Clear Day";
                song.artist = "bensound.com";
                song.image = R.drawable.meow;
                song.source = R.raw.clearday;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Energy";
                song.artist = "bensound.com";
                song.image = R.drawable.meow;
                song.source = R.raw.energy;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Funky Element";
                song.artist = "bensound.com";
                song.image = R.drawable.meow;
                song.source = R.raw.funkyelement;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Hey";
                song.artist = "bensound.com";
                song.image = R.drawable.meow;
                song.source = R.raw.heypop;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Inspire";
                song.artist = "bensound.com";
                song.image = R.drawable.meow;
                song.source = R.raw.inspire;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Little Idea";
                song.artist = "bensound.com";
                song.image = R.drawable.meow;
                song.source = R.raw.littleidea;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Sweet";
                song.artist = "bensound.com";
                song.image = R.drawable.meow;
                song.source = R.raw.sweet;
                playlist.songs.add(song);
                break;

            case 4:
                // playlist
                playlist.name = "Rock";
                playlist.songs = new ArrayList<Music>();
                // songs
                System.out.println("Rock");

                song = new Music();
                song.name = "All That";
                song.artist = "bensound.com";
                song.image = R.drawable.meow;
                song.source = R.raw.allthat;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Hip Jazz";
                song.artist = "bensound.com";
                song.image = R.drawable.meow;
                song.source = R.raw.hipjazz;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Jazz Comedy";
                song.artist = "bensound.com";
                song.image = R.drawable.meow;
                song.source = R.raw.jazzcomedy;
                playlist.songs.add(song);


                song = new Music();
                song.name = "Jazzy Frenchy";
                song.artist = "bensound.com";
                song.image = R.drawable.meow;
                song.source = R.raw.jazzyfrenchy;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Love";
                song.artist = "bensound.com";
                song.image = R.drawable.meow;
                song.source = R.raw.love;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Romantic";
                song.artist = "bensound.com";
                song.image = R.drawable.meow;
                song.source = R.raw.romantic;
                playlist.songs.add(song);

                song = new Music();
                song.name = "THe Jazz Piano";
                song.artist = "bensound.com";
                song.image = R.drawable.meow;
                song.source = R.raw.thejazzpiano;
                playlist.songs.add(song);

                song = new Music();
                song.name = "The Lounge";
                song.artist = "bensound.com";
                song.image = R.drawable.meow;
                song.source = R.raw.thelounge;
                playlist.songs.add(song);
                break;

            case 5:
                // playlist
                playlist.name = "Meditate";
                playlist.songs = new ArrayList<Music>();
                // songs
                System.out.println("Meditate/Calm");

                song = new Music();
                song.name = "Beautiful Dream";
                song.artist = "bensound.com";
                song.image = R.drawable.meditate;
                song.source = R.raw.beautifuldream;
                playlist.songs.add(song);

                song = new Music();
                song.name = "CBPD";
                song.artist = "bensound.com";
                song.image = R.drawable.meditate;
                song.source = R.raw.cbpd;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Fun and Games";
                song.artist = "bensound.com";
                song.image = R.drawable.meditate;
                song.source = R.raw.funandgames;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Games Worldbeat";
                song.artist = "bensound.com";
                song.image = R.drawable.meditate;
                song.source = R.raw.gamesworldbeat;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Green Chair";
                song.artist = "bensound.com";
                song.image = R.drawable.meditate;
                song.source = R.raw.greenchair;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Just Chill";
                song.artist = "bensound.com";
                song.image = R.drawable.meditate;
                song.source = R.raw.justchill;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Latin Lovers";
                song.artist = "bensound.com";
                song.image = R.drawable.meditate;
                song.source = R.raw.latinlovers;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Playful";
                song.artist = "bensound.com";
                song.image = R.drawable.meditate;
                song.source = R.raw.playful;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Romantic 759";
                song.artist = "bensound.com";
                song.image = R.drawable.meditate;
                song.source = R.raw.romantic759;
                playlist.songs.add(song);
                song = new Music();

                song.name = "Romantic 659";
                song.artist = "bensound.com";
                song.image = R.drawable.meditate;
                song.source = R.raw.romantic659;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Silent Decent";
                song.artist = "bensound.com";
                song.image = R.drawable.meditate;
                song.source = R.raw.silentdecent;
                playlist.songs.add(song);
                break;

            case 6:
                // playlist
                playlist.name = "Workout";
                playlist.songs = new ArrayList<Music>();
                // songs
                System.out.println("Workout");

                song = new Music();
                song.name = "A Game";
                song.artist = "bensound.com";
                song.image = R.drawable.workout;
                song.source = R.raw.agame;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Be Happy";
                song.artist = "bensound.com";
                song.image = R.drawable.workout;
                song.source = R.raw.behappy;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Deep Urban";
                song.artist = "bensound.com";
                song.image = R.drawable.workout;
                song.source = R.raw.deepurban;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Disco Ain't Old School";
                song.artist = "bensound.com";
                song.image = R.drawable.workout;
                song.source = R.raw.discoaintoldschool;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Driving Ambition";
                song.artist = "bensound.com";
                song.image = R.drawable.workout;
                song.source = R.raw.drivingambition;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Gear";
                song.artist = "bensound.com";
                song.image = R.drawable.workout;
                song.source = R.raw.gear;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Hey (AGAIN, WHY)";
                song.artist = "bensound.com";
                song.image = R.drawable.workout;
                song.source = R.raw.hey3;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Hiding Under";
                song.artist = "bensound.com";
                song.image = R.drawable.workout;
                song.source = R.raw.hidingunder;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Keeping Fit";
                song.artist = "bensound.com";
                song.image = R.drawable.workout;
                song.source = R.raw.keepingfit;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Minimal Techno";
                song.artist = "bensound.com";
                song.image = R.drawable.workout;
                song.source = R.raw.minimaltechno;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Motivating Mornings";
                song.artist = "bensound.com";
                song.image = R.drawable.workout;
                song.source = R.raw.motivatingmornings;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Positive Energy";
                song.artist = "bensound.com";
                song.image = R.drawable.workout;
                song.source = R.raw.positiveenergy;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Ride The Waves";
                song.artist = "bensound.com";
                song.image = R.drawable.workout;
                song.source = R.raw.ridethewaves;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Rising Forest";
                song.artist = "bensound.com";
                song.image = R.drawable.workout;
                song.source = R.raw.risingforest;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Super Strong";
                song.artist = "bensound.com";
                song.image = R.drawable.workout;
                song.source = R.raw.superstrong;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Surfs Up dude";
                song.artist = "bensound.com";
                song.image = R.drawable.workout;
                song.source = R.raw.surfsupdude;
                playlist.songs.add(song);

                song = new Music();
                song.name = "The Greatest Comeback";
                song.artist = "bensound.com";
                song.image = R.drawable.workout;
                song.source = R.raw.thegreatestcomeback;
                playlist.songs.add(song);

                song = new Music();
                song.name = "To The Next Round";
                song.artist = "bensound.com";
                song.image = R.drawable.workout;
                song.source = R.raw.tothenextround;
                playlist.songs.add(song);
                break;

            case 7:
                // playlist
                playlist.name = "Rap";
                playlist.songs = new ArrayList<Music>();
                // songs
                System.out.println("Rap");

                song = new Music();
                song.name = "Rap By Infraction Preview";
                song.artist = "bensound.com";
                song.image = R.drawable.rap;
                song.source = R.raw.rapbyinfractionpreview;
                playlist.songs.add(song);


                song = new Music();
                song.name = "Epic";
                song.artist = "bensound.com";
                song.image = R.drawable.rap;
                song.source = R.raw.epic;
                playlist.songs.add(song);


                song = new Music();
                song.name = "Evollution One";
                song.artist = "bensound.com";
                song.image = R.drawable.rap;
                song.source = R.raw.evolution;
                playlist.songs.add(song);


                song = new Music();
                song.name = "Evolution Two";
                song.artist = "bensound.com";
                song.image = R.drawable.rap;
                song.source = R.raw.evolution2;
                playlist.songs.add(song);


                song = new Music();
                song.name = "Going higher";
                song.artist = "bensound.com";
                song.image = R.drawable.rap;
                song.source = R.raw.evolution3;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Evolution Four";
                song.artist = "bensound.com";
                song.image = R.drawable.rap;
                song.source = R.raw.evolution4;
                playlist.songs.add(song);


                song = new Music();
                song.name = "Groovy Hiphop";
                song.artist = "bensound.com";
                song.image = R.drawable.rap;
                song.source = R.raw.groovyhiphop;
                playlist.songs.add(song);


                song = new Music();
                song.name = "Thug Life";
                song.artist = "bensound.com";
                song.image = R.drawable.rap;
                song.source = R.raw.thuglife;
                playlist.songs.add(song);


                song = new Music();
                song.name = "Astrology";
                song.artist = "bensound.com";
                song.image = R.drawable.rap;
                song.source = R.raw.astrology;
                playlist.songs.add(song);


                song = new Music();
                song.name = "Boulevard 422";
                song.artist = "bensound.com";
                song.image = R.drawable.rap;
                song.source = R.raw.boulevard422;
                playlist.songs.add(song);


                song = new Music();
                song.name = "Bruh";
                song.artist = "bensound.com";
                song.image = R.drawable.rap;
                song.source = R.raw.bruh;
                playlist.songs.add(song);


                song = new Music();
                song.name = "Crank In Up Non Stop";
                song.artist = "bensound.com";
                song.image = R.drawable.rap;
                song.source = R.raw.crankinupnonstop;
                playlist.songs.add(song);


                song = new Music();
                song.name = "Dont You Know Its True";
                song.artist = "bensound.com";
                song.image = R.drawable.rap;
                song.source = R.raw.dontyouknowitstrue951;
                playlist.songs.add(song);


                song = new Music();
                song.name = "Dream Land";
                song.artist = "bensound.com";
                song.image = R.drawable.rap;
                song.source = R.raw.dreamland401;
                playlist.songs.add(song);


                song = new Music();
                song.name = "Dream Land Two";
                song.artist = "bensound.com";
                song.image = R.drawable.rap;
                song.source = R.raw.dreamland401one;
                playlist.songs.add(song);


                song = new Music();
                song.name = "Gotta Get It";
                song.artist = "bensound.com";
                song.image = R.drawable.rap;
                song.source = R.raw.gottagetit448;
                playlist.songs.add(song);


                song = new Music();
                song.name = "Greed";
                song.artist = "bensound.com";
                song.image = R.drawable.rap;
                song.source = R.raw.greed;
                playlist.songs.add(song);


                song = new Music();
                song.name = "Its Like That";
                song.artist = "bensound.com";
                song.image = R.drawable.rap;
                song.source = R.raw.itslikethat;
                playlist.songs.add(song);


                song = new Music();
                song.name = "Its On You";
                song.artist = "bensound.com";
                song.image = R.drawable.rap;
                song.source = R.raw.itsonyou;
                playlist.songs.add(song);


                song = new Music();
                song.name = "King Paranoia";
                song.artist = "bensound.com";
                song.image = R.drawable.rap;
                song.source = R.raw.kingparanoia;
                playlist.songs.add(song);


                song = new Music();
                song.name = "K K 407";
                song.artist = "bensound.com";
                song.image = R.drawable.rap;
                song.source = R.raw.kk407;
                playlist.songs.add(song);


                song = new Music();
                song.name = "Like A Loop Machine";
                song.artist = "bensound.com";
                song.image = R.drawable.rap;
                song.source = R.raw.likealoopmachine;
                playlist.songs.add(song);


                song = new Music();
                song.name = "Move Your Body";
                song.artist = "bensound.com";
                song.image = R.drawable.rap;
                song.source = R.raw.moveyourbody;
                playlist.songs.add(song);


                song = new Music();
                song.name = "Praise The Lord";
                song.artist = "bensound.com";
                song.image = R.drawable.rap;
                song.source = R.raw.praisethelord;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Praise The Lord Two";
                song.artist = "bensound.com";
                song.image = R.drawable.rap;
                song.source = R.raw.praisethelord2;
                playlist.songs.add(song);

                song = new Music();
                song.name = "Purple JS";
                song.artist = "bensound.com";
                song.image = R.drawable.rap;
                song.source = R.raw.purplejs;
                playlist.songs.add(song);
                break;

            default:
                System.out.println("No Music???");
                break;
            }
        }

    void connectXMLViews() {
        playlistRecyclerView = findViewById(R.id.recycler_view);
        songName = findViewById(R.id.song_name);
        songImage = findViewById(R.id.song_image);
        buttonPlay = findViewById(R.id.play);
        buttonSkipNext = findViewById(R.id.skip_next);
        buttonSkipPrevious = findViewById(R.id.skip_previous);
        artistName = findViewById(R.id.artist_name);
    }
    void connectPlaylistViews() {
        pop = findViewById(R.id.pop);
        rock = findViewById(R.id.rock_songs);
        jazz = findViewById(R.id.jazz_playlist);
        main = findViewById(R.id.main_songs);
        meditate = findViewById(R.id.meditate_playlist);
        workout = findViewById(R.id.workout_playlist);
        rap = findViewById(R.id.rap);
    }
    void displayCurrentSong() {
        Music currentSong = playlist.songs.get(currentSongIndex);
        songImage.setImageResource(currentSong.image);
        artistName.setText(currentSong.artist);
        songName.setText(currentSong.name);
    }
    void setupButtonHandlers() {
        buttonSkipNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("next");
                if (currentSongIndex + 1 < playlist.songs.size()) {
                    switchSong(currentSongIndex, currentSongIndex + 1);
                }
            }
        });
        buttonSkipPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("prev");
                if (currentSongIndex - 1 >= 0) {
                    switchSong(currentSongIndex, currentSongIndex - 1);
                }
            }
        });
        buttonPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("play");
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    pauseSong();
                } else {
                    playSong();
                }
                    // change the layout in XML
            }
        });
    }
    void switchSong(int fromIndex, int toIndex) {
        songAdapter.notifyItemChanged(currentSongIndex);

        currentSongIndex = toIndex;

        displayCurrentSong();

        songAdapter.notifyItemChanged(currentSongIndex);

        playlistRecyclerView.scrollToPosition(currentSongIndex);

        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            pauseSong();
            buttonPlay.setImageResource(R.drawable.ic_play);
            mediaPlayer = null;
            playSong();
            buttonPlay.setImageResource(R.drawable.ic_pause);
        } else {
            mediaPlayer = null;
        }
    }
    void playSong() {
        // lol
        System.out.println("cool, playing...");
        if (mediaPlayer == null) {
            Music currentSong = playlist.songs.get(currentSongIndex);
            mediaPlayer = MediaPlayer.create(MainActivity.this, currentSong.source);
            buttonPlay.setImageResource(R.drawable.ic_pause);
        }
        mediaPlayer.start();
        buttonPlay.setImageResource(R.drawable.ic_pause);
    }
    void pauseSong() {
        System.out.println("cool, pausing...");
        if (mediaPlayer != null) {
            mediaPlayer.pause();
            buttonPlay.setImageResource(R.drawable.ic_play);
        }
    }

    MediaPlayer mediaPlayer = null;

    Playlist playlist = new Playlist();
    Integer currentSongIndex = 0;

    SongAdapter songAdapter;

    //XML views
    RecyclerView playlistRecyclerView;
    ImageView songImage;
    ImageButton buttonSkipPrevious;
    ImageButton buttonSkipNext;
    ImageButton buttonPlay;
    TextView artistName;
    TextView songName;
    ImageButton jazz;
    ImageButton main;
    ImageButton pop;
    ImageButton rock;
    ImageButton rap;
    ImageButton workout;
    ImageButton meditate;
}