package com.example.test01;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SongViewHolder extends RecyclerView.ViewHolder {

    public SongViewHolder(@NonNull View itemView) {
        super(itemView);
        this.itemView = itemView;
        imageView = itemView.findViewById(R.id.image_view);
        songNameView = itemView.findViewById(R.id.text_song_name);
        artistNameView = itemView.findViewById(R.id.text_artist_name);
        selectedBackGround = itemView.findViewById(R.id.selectedBackground);
    }

    View itemView;
    ImageView imageView;
    TextView songNameView;
    TextView artistNameView;
    View selectedBackGround;
}
